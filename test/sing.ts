import { expect } from "chai";
import { ethers } from "hardhat";
import { Wallet } from 'ethers'
import web3  from 'web3'
// import { ContractFactory } from './share/ContractFactory'
let owner: Wallet, 
    admin1: Wallet,
    admin2: Wallet,
    employee1: Wallet,
    employee2: Wallet,
    cashier1: Wallet,
    cashier2: Wallet,
    sing:any,
    CASHIER_ROLE:string = "0xff3317e1e0e8c784290b957ee9b65ea8fe96420c4819db463283b95ecbe4a3fe",
    CUSTOMERS_ROLE:string = "0xaf6786efc154b345802554f1dea27e60ea4c3393b6b38eb8438e22c20e088bd2",
    EMPLOYEE_ROLE:string = "0x5a3425ea67e73c95853ecfc0c443062de292a303c186bc2aac1178ae1b86c62a",
    DEFAULT_ADMIN_ROLE:string = "0x0000000000000000000000000000000000000000000000000000000000000000";

describe("sing",  () =>{

  before("create fixture loader", async () => {
    ;[  
      admin1,
      admin2,
      employee1,
      employee2,
      cashier1,
      cashier2,
      owner
    ] = await (ethers as any).getSigners()
    const  Sigs = await ethers.getContractFactory("Sigs",admin1);
    sing = await  Sigs.deploy(owner.address)
    await sing.deployed();
    

  })
  it("test signature",async () =>{
        let message =  ethers.utils.solidityKeccak256(
            [ "address", "uint256" , "uint256", "address" ],
            [admin1.address, "1", "0",sing.address ]
        );

       let  dataHashBin = ethers.utils.arrayify(message)
       let signature = await owner.signMessage(dataHashBin);
        console.log("address",admin1.address)
      await sing.transfer("1","0",signature)
  })




  
});


