import { expect } from "chai";
import { ethers } from "hardhat";
import { Wallet } from 'ethers'
// import { ContractFactory } from './share/ContractFactory'
let adminProxsy: Wallet, 
    admin: Wallet,
    financial1: Wallet,
    financial2: Wallet,
    employee1: Wallet,
    employee2: Wallet,
    cashier1: Wallet,
    cashier2: Wallet,
    customer1: Wallet,
    customer2: Wallet,
    adminupgradeabilityProxy:any,
    brs:any,
    CASHIER_ROLE:string = "0xff3317e1e0e8c784290b957ee9b65ea8fe96420c4819db463283b95ecbe4a3fe",
    EMPLOYEE_ROLE:string = "0x5a3425ea67e73c95853ecfc0c443062de292a303c186bc2aac1178ae1b86c62a",
    DEFAULT_ADMIN_ROLE:string = "0x0000000000000000000000000000000000000000000000000000000000000000",
    FINANCIAL_ROLE:string = "0x5245999133c6d8d4571d82773bdec82e2a254d49eae8d487e9867b56643d7333",
    log:boolean = true,
    mint_start = ethers.utils.parseUnits('3000000000', 18);
    ;
describe.only("Upgradeable  token Siam Brasseries",  () =>{
  before("create fixture loader", async () => {
    ;[  
      financial1,
      financial2,
      employee1,
      employee2,
      cashier1,
      cashier2,
      adminProxsy,
      admin,
      customer1,
      customer2
    ] = await (ethers as any).getSigners()
    const  AdminUpgradeabilityProxy = await ethers.getContractFactory("AdminUpgradeabilityProxy");
    const  BRS = await ethers.getContractFactory("BRS");
    brs = await  BRS.deploy()
    await brs.deployed();
    
    adminupgradeabilityProxy =await AdminUpgradeabilityProxy.deploy(brs.address,adminProxsy.address,"0x")

    await adminupgradeabilityProxy.deployed();
  })

  
  it("initialize token",async () =>{
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,admin);
    let name =  await brs_token.name()
    await( await brs_token.initialize(mint_start)).wait()
    name =  await brs_token.name()
    let balance_admin = await brs_token.balanceOf(admin.address);
    expect(name).to.equal("Siam Brasseries")
    expect(balance_admin).to.equal(mint_start)
  })
  it("add role",async () =>{
        const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,admin);
        await checkrole(brs_token,"befor")
        await( await brs_token.grantRole(EMPLOYEE_ROLE,employee1.address)).wait()
        await( await brs_token.grantRole(EMPLOYEE_ROLE,employee2.address)).wait()
        await( await brs_token.grantRole(CASHIER_ROLE,cashier1.address)).wait()
        await( await brs_token.grantRole(CASHIER_ROLE,cashier2.address)).wait()
        await( await brs_token.grantRole(FINANCIAL_ROLE,financial1.address)).wait()
        await( await brs_token.grantRole(FINANCIAL_ROLE,financial2.address)).wait()
        await checkrole(brs_token,"after")

        console.log(
          await brs_token.isAdmin(employee1.address),
          await brs_token.isCashier(cashier1.address),
          await brs_token.isEmployee(employee1.address),
          await brs_token.isFinancial(financial2.address),
        )
  })
  // it("mint token to admin",async () => {
  //     const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,admin)
  //     await ( await brs_token.mint(admin.address,ethers.utils.parseUnits('100', 18) ) ).wait()
  //     let balance_financial1 = await brs_token.balanceOf(admin.address);
  //     fnlog(`
  //       balance_admin : ${ethers.utils.formatUnits(balance_financial1,18)}
  //     `)
  // })

  it("tranfer admin to financial",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,admin)
    await (await brs_token.transfer(financial1.address,ethers.utils.parseUnits('2000000000', 18))).wait()
    let balance_financial1 =  await brs_token.balanceOf(financial1.address)
    let balance_admin =  await brs_token.balanceOf(admin.address)
    expect(balance_financial1).to.equal(ethers.utils.parseUnits('2000000000', 18))
    expect(balance_admin).to.equal(ethers.utils.parseUnits('1000000000', 18))
    fnlog(`
    balance_financial1 : ${ethers.utils.formatUnits(balance_financial1,18)}
    balance_admin : ${ethers.utils.formatUnits(balance_admin,18)}
     `)
  })
  it("tranfer financial to financial ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,financial1)
    await (await brs_token.transfer(financial2.address,ethers.utils.parseUnits('1000000000', 18))).wait()
    let balance_financial2 =  await brs_token.balanceOf(financial2.address)
    let balance_financial1 =  await brs_token.balanceOf(admin.address)
    expect(balance_financial2).to.equal(ethers.utils.parseUnits('1000000000', 18))
    expect(balance_financial1).to.equal(ethers.utils.parseUnits('1000000000', 18))
    fnlog(`
    balance_financial1 : ${ethers.utils.formatUnits(balance_financial1,18)}
    balance_financial2 : ${ethers.utils.formatUnits(balance_financial2,18)}
     `)
  })
  it("tranfer financial to  employee ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,financial1)
    await (await brs_token.transfer(employee1.address,ethers.utils.parseUnits('100000', 18))).wait()
    await (await brs_token.transfer(employee2.address,ethers.utils.parseUnits('100000', 18))).wait()
    let balance_employee1 =  await brs_token.balanceOf(employee1.address)
    let balance_employee2 =  await brs_token.balanceOf(employee2.address)
    expect(balance_employee1).to.equal(ethers.utils.parseUnits('100000', 18))
    expect(balance_employee2).to.equal(ethers.utils.parseUnits('100000', 18))
    fnlog(`
    balance_balance_employee1 : ${ethers.utils.formatUnits(balance_employee1,18)}
    balance_balance_employee2 : ${ethers.utils.formatUnits(balance_employee2,18)}
     `)
  })
  it("tranfer employee to  employee ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,employee1)
    try {
      await (await brs_token.transfer(employee2.address,ethers.utils.parseUnits('100000', 18))).wait()
    } catch (error) {
    }
    let balance_employee1 =  await brs_token.balanceOf(employee1.address)
    let balance_employee2 =  await brs_token.balanceOf(employee2.address)
    expect(balance_employee1).to.equal(ethers.utils.parseUnits('100000', 18))
    expect(balance_employee2).to.equal(ethers.utils.parseUnits('100000', 18))
    fnlog(`
    balance_balance_employee1 : ${ethers.utils.formatUnits(balance_employee1,18)}
    balance_balance_employee2 : ${ethers.utils.formatUnits(balance_employee2,18)}
     `)
  })
  it("tranfer employee to  cutomer ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,employee2)
    try {
      await (await brs_token.transfer(customer1.address,ethers.utils.parseUnits('100000', 18))).wait()
    } catch (error) {
    }
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    let balance_employee2 =  await brs_token.balanceOf(employee2.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('0', 18))
    expect(balance_employee2).to.equal(ethers.utils.parseUnits('100000', 18))
    fnlog(`
    balance_balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
    balance_balance_employee2 : ${ethers.utils.formatUnits(balance_employee2,18)}
     `)
  })
  it("tranfer employee to  financial ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,employee1)
    try {
      await (await brs_token.transfer(financial2.address,ethers.utils.parseUnits('100000', 18))).wait()
    } catch (error) {
    }
    let balance_employee1 =  await brs_token.balanceOf(employee1.address)
    let balance_financial2 =  await brs_token.balanceOf(financial2.address)
    expect(balance_employee1).to.equal(ethers.utils.parseUnits('0', 18))
    expect(balance_financial2).to.equal(ethers.utils.parseUnits('1000100000', 18))
    fnlog(`
    balance_balance_employee1 : ${ethers.utils.formatUnits(balance_employee1,18)}
    balance_balance_financial2 : ${ethers.utils.formatUnits(balance_financial2,18)}
     `)
  })
  it("tranfer employee to  admin ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,employee2)
    await (await brs_token.transfer(admin.address,ethers.utils.parseUnits('10000', 18))).wait()
    let balance_employee2 =  await brs_token.balanceOf(employee2.address)
    let balance_admin =  await brs_token.balanceOf(admin.address)
    expect(balance_employee2).to.equal(ethers.utils.parseUnits('90000', 18))
    expect(balance_admin).to.equal(ethers.utils.parseUnits('1000010000', 18))
    fnlog(`
    balance_balance_employee2 : ${ethers.utils.formatUnits(balance_employee2,18)}
    balance_balance_admin : ${ethers.utils.formatUnits(balance_admin,18)}
     `)
  })
  it("tranfer employee to  cashier ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,employee2)
    await (await brs_token.transfer(cashier1.address,ethers.utils.parseUnits('10000', 18))).wait()
    let balance_employee2 =  await brs_token.balanceOf(employee2.address)
    let balance_cashier1 =  await brs_token.balanceOf(cashier1.address)
    expect(balance_employee2).to.equal(ethers.utils.parseUnits('80000', 18))
    expect(balance_cashier1).to.equal(ethers.utils.parseUnits('10000', 18))
    fnlog(`
    balance_balance_employee2 : ${ethers.utils.formatUnits(balance_employee2,18)}
    balance_balance_cashier1 : ${ethers.utils.formatUnits(balance_cashier1,18)}
     `)
  })
  it("tranfer cashier to  cashier ",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,cashier1)
    await (await brs_token.transfer(cashier2.address,ethers.utils.parseUnits('10000', 18))).wait()
    let balance_cashier2 =  await brs_token.balanceOf(cashier2.address)
    let balance_cashier1 =  await brs_token.balanceOf(cashier1.address)
    expect(balance_cashier1).to.equal(ethers.utils.parseUnits('0', 18))
    expect(balance_cashier2).to.equal(ethers.utils.parseUnits('10000', 18))
    fnlog(`
    balance_balance_cashier1 : ${ethers.utils.formatUnits(balance_cashier1,18)}
    balance_balance_cashier2 : ${ethers.utils.formatUnits(balance_cashier2,18)}
     `)
  })
  it("tranfer cashier to  financial",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,cashier2)
    await (await brs_token.transfer(financial1.address,ethers.utils.parseUnits('1000', 18))).wait()
    let balance_cashier2 =  await brs_token.balanceOf(cashier2.address)
    let balance_financial1 =  await brs_token.balanceOf(financial1.address)
    expect(balance_financial1).to.equal(ethers.utils.parseUnits('999801000', 18))
    expect(balance_cashier2).to.equal(ethers.utils.parseUnits('9000', 18))
    fnlog(`
    balance_balance_financial1 : ${ethers.utils.formatUnits(balance_financial1,18)}
    balance_balance_cashier2 : ${ethers.utils.formatUnits(balance_cashier2,18)}
     `)
  })
  it("tranfer cashier to  admin",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,cashier2)
    await (await brs_token.transfer(admin.address,ethers.utils.parseUnits('1000', 18))).wait()
    let balance_cashier2 =  await brs_token.balanceOf(cashier2.address)
    let balance_admin =  await brs_token.balanceOf(admin.address)
    expect(balance_admin).to.equal(ethers.utils.parseUnits('1000011000', 18))
    expect(balance_cashier2).to.equal(ethers.utils.parseUnits('8000', 18))
    fnlog(`
    balance_balance_admin : ${ethers.utils.formatUnits(balance_admin,18)}
    balance_balance_cashier2 : ${ethers.utils.formatUnits(balance_cashier2,18)}
     `)
  })
  it("tranfer cashier to  employee",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,cashier2)
    try {
      await (await brs_token.transfer(employee1.address,ethers.utils.parseUnits('1000', 18))).wait()
    } catch (error) {
      
    }
    let balance_cashier2 =  await brs_token.balanceOf(cashier2.address)
    expect(balance_cashier2).to.equal(ethers.utils.parseUnits('8000', 18))
    fnlog(`
    balance_balance_cashier2 : ${ethers.utils.formatUnits(balance_cashier2,18)}
     `)
  })
  
  it("tranfer cashier to  customer",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,cashier2)
    try {
      await (await brs_token.transfer(customer1.address,ethers.utils.parseUnits('1000', 18))).wait()
    } catch (error) {
      
    }
    let balance_cashier2 =  await brs_token.balanceOf(cashier2.address)
    expect(balance_cashier2).to.equal(ethers.utils.parseUnits('8000', 18))
    fnlog(`
    balance_balance_cashier2 : ${ethers.utils.formatUnits(balance_cashier2,18)}
     `)
  })
  it("tranfer admin to customer",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,admin)
    await (await brs_token.transfer(customer1.address,ethers.utils.parseUnits('1000', 18))).wait()
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('1000', 18))
    fnlog(`
    balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
     `)
  })

  it("tranfer customer to cashier",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,customer1)
    await (await brs_token.transfer(cashier1.address,ethers.utils.parseUnits('100', 18))).wait()
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('900', 18))
    fnlog(`
    balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
     `)
  })

  it("tranfer customer to employee",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,customer1)
    try {
      await (await brs_token.transfer(employee1.address,ethers.utils.parseUnits('100', 18))).wait()
    } catch (error) {
    }
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('900', 18))
    fnlog(`
    balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
     `)
  })
  it("tranfer customer to customer",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,customer1)
    try {
      await (await brs_token.transfer(customer2.address,ethers.utils.parseUnits('100', 18))).wait()
    } catch (error) {
    }
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('900', 18))
    fnlog(`
    balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
     `)
  })
  it("tranfer customer to financial",async () => {
    const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,customer1)
    try {
      await (await brs_token.transfer(financial1.address,ethers.utils.parseUnits('100', 18))).wait()
    } catch (error) {
    }
    let balance_customer1 =  await brs_token.balanceOf(customer1.address)
    expect(balance_customer1).to.equal(ethers.utils.parseUnits('900', 18))
    fnlog(`
    balance_customer1 : ${ethers.utils.formatUnits(balance_customer1,18)}
     `)
  })


  
});

async function checkrole(brs_token:any,ac:string) {
  if(!log) return; 
  const t1 =  await brs_token.getRoleMemberCount(CASHIER_ROLE)
  const t2 =  await brs_token.getRoleMemberCount(EMPLOYEE_ROLE)
  const t3 =  await brs_token.getRoleMemberCount(FINANCIAL_ROLE)
  fnlog(`
    ${ac}
      CASHIER_ROLE  ${t1}
      EMPLOYEE_ROLE ${t2}
      FINANCIAL_ROLE ${t3}
  `)
}


function fnlog(d:string){
  if(!log) return; 
  console.log('\x1b[33m%s\x1b[0m',d)
}
