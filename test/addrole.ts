import { expect } from "chai";
import { ethers } from "hardhat";
import { Wallet } from 'ethers'
// import { ContractFactory } from './share/ContractFactory'
type address = string;
let adminProxsy: Wallet, 
    admin: Wallet,
    financial1: Wallet,
    financial2: Wallet,
    employee1: Wallet,
    employee2: Wallet,
    cashier1: Wallet,
    cashier2: Wallet,
    customer1: Wallet,
    customer2: Wallet,
    adminupgradeabilityProxy:any,
    brs:any,
    CASHIER_ROLE:string = "0xff3317e1e0e8c784290b957ee9b65ea8fe96420c4819db463283b95ecbe4a3fe",
    EMPLOYEE_ROLE:string = "0x5a3425ea67e73c95853ecfc0c443062de292a303c186bc2aac1178ae1b86c62a",
    DEFAULT_ADMIN_ROLE:string = "0x0000000000000000000000000000000000000000000000000000000000000000",
    FINANCIAL_ROLE:string = "0x5245999133c6d8d4571d82773bdec82e2a254d49eae8d487e9867b56643d7333",
    log:boolean = true,
    roleAddress = [
        {name:'admin',role:[
          "0x6449B2F26CF7f0e9636b7aa82770392D5e0a5200",
          "0x4eABa1009837AE0E4282f5bfafec128e075498FB",
    
        ]},
        {name:'cashier',role:[
          "0x214aD830305A9048c008EE8B1916e3c6A4D185C0",
          "0x9E36D8a3d1D6cAdA88d9cEB5aa07c59e24d88C8b",
        ]},
        {name:'employee',role:[
          '0x050a24095542B0be1ECFA0B7c5920F99dB405a0b',
          '0x41015061B2fe76CF7F6d7a075Cc047F987b378F9',
          '0xbC44D3e7Ead2751ce3F25059dbEAef245f47867e',
          '0x2724089F91CdD7f406300F72487f7f0a0567f464',
          '0x3CbB8d53CB58c69b10BF5CEff1afC244aFF39e34',
        ]},
        {name:'financial',role:[
          "0x49A0d1e83d1b5a8F4F1A6A33cF203bD92F57fAE6",
          "0x2144CEDb38B0795527d63361603fFA75050c2407"
        ]},
    ]
    ;
describe("add role  token Siam Brasseries",  () =>{
  before("create fixture loader", async () => {
    ;[  
      financial1,
      financial2,
      employee1,
      employee2,
      cashier1,
      cashier2,
      adminProxsy,
      admin,
      customer1,
      customer2
    ] = await (ethers as any).getSigners()
  })
  
  it("add role",async () =>{
        let ALlrole:any = {
          admin:DEFAULT_ADMIN_ROLE,
          cashier:CASHIER_ROLE,
          employee:EMPLOYEE_ROLE,
          financial:FINANCIAL_ROLE,
        }
        const  brs_token = await ethers.getContractAt("BRS",'0x7e577309e1cD5aE872cbEfaDE4418A893c0cFe2F',admin);
        // const role = await brs_token.hasRole(ALlrole.admin,addemployee.rol)
        // console.log(role)
        var result = Object.keys(ALlrole);
        // console.log(result.filter(vl=>vl!='admin'))
        for (let index = 0; index < roleAddress.length; index++) {
          const roleaddress = roleAddress[index];
          // console.log(roleaddress.name)
          for (let index = 0; index < roleaddress.role.length; index++) {
            const address = roleaddress.role[index];
            // console.log(roleaddress.name,address)
              for (let index = 0; index < result.length; index++) {
                const role = ALlrole[result[index]];
                const isrole = await brs_token.hasRole(role,address)
                if(isrole){
                  const balance = await brs_token.balanceOf(address)
                    console.log(`
                       balance : ${ethers.utils.formatUnits(balance, 18)}
                       isrole :  ${result[index]}
                       cuan : ${roleaddress.name}
                       address : ${address}
                    `)
                }

              }
          }
        }

        // await checkrole(brs_token,"befor")
        // for (let index = 0; index < addemployee.length; index++) {
        //   const address = addemployee[index];
        //   await( await brs_token.grantRole(EMPLOYEE_ROLE,address)).wait()
        // }
        // for (let index = 0; index < addcashier.length; index++) {
        //   const address = addcashier[index];
        //   await( await brs_token.grantRole(CASHIER_ROLE,address)).wait()
        // }
        // for (let index = 0; index < addfinancial.length; index++) {
        //   const address = addfinancial[index];
        //   await( await brs_token.grantRole(FINANCIAL_ROLE,address)).wait()
        // }
        // for (let index = 0; index < addadmin.length; index++) {
        //   const address = addadmin[index];
        //   await( await brs_token.grantRole(DEFAULT_ADMIN_ROLE,address)).wait()
        // }
        // await checkrole(brs_token,"after")
  })
  

  
});

async function checkrole(brs_token:any,ac:string) {
  if(!log) return; 
  const t1 =  await brs_token.getRoleMemberCount(CASHIER_ROLE)
  const t2 =  await brs_token.getRoleMemberCount(EMPLOYEE_ROLE)
  const t3 =  await brs_token.getRoleMemberCount(FINANCIAL_ROLE)
  fnlog(`
    ${ac}
      CASHIER_ROLE  ${t1}
      EMPLOYEE_ROLE ${t2}
      FINANCIAL_ROLE ${t3}
  `)
}


function fnlog(d:string){
  if(!log) return; 
  console.log('\x1b[33m%s\x1b[0m',d)
}
