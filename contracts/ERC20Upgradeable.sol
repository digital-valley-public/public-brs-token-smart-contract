// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./interfaces/IERC20.sol";
import "./interfaces/IERC20Metadata.sol";
import "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";

contract ERC20Upgradeable is Initializable, ContextUpgradeable, IERC20,IERC20Metadata,AccessControlEnumerableUpgradeable {
    

    using ECDSA for bytes32;

    bytes32 public constant FINANCIAL_ROLE = keccak256("FINANCIAL_ROLE");
    bytes32 public constant EMPLOYEE_ROLE = keccak256("EMPLOYEE_ROLE");
    bytes32 public constant CASHIER_ROLE = keccak256("CASHIER_ROLE");

    mapping(address => uint256) private _balances;

    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 private _totalSupply;

    string private _name;
    string private _symbol;

    mapping(address => mapping(uint256 => bool)) Nonces;
    mapping(address => uint256) MostForNonces;

  
    function isAdmin(address _account) public view returns(bool role){
        return hasRole(DEFAULT_ADMIN_ROLE, _account);
    }
    function isFinancial(address _account) public view returns(bool role){
        return hasRole(FINANCIAL_ROLE, _account);
    }
    function isEmployee(address _account) public view returns(bool role){
        return hasRole(EMPLOYEE_ROLE, _account);
    }
    function isCashier(address _account) public view returns(bool role){
        return hasRole(CASHIER_ROLE, _account);
    }
    function isCustomer(address _account) public view returns(bool role){
        return (hasRole(DEFAULT_ADMIN_ROLE, _account))
        ? false :
        (hasRole(FINANCIAL_ROLE, _account)) 
        ? false :
        (hasRole(EMPLOYEE_ROLE, _account))
        ? false :
        (hasRole(CASHIER_ROLE, _account))
        ? false :
        true;
    }

    function __ERC20_init(string memory name_, string memory symbol_, uint256 mint_) internal onlyInitializing {
        __ERC20_init_unchained(name_, symbol_,mint_);
    }

    function __ERC20_init_unchained(string memory name_, string memory symbol_,uint256 mint_) internal onlyInitializing {
        _name = name_;
        _symbol = symbol_;
        _mint(_msgSender(),mint_);
    }

    function name() public view virtual override returns (string memory) {
        return _name;
    }

    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    function transfer(address to, uint256 amount) public virtual override returns (bool) {
        
        address owner = _msgSender();
        _transfer(owner, to, amount);
        
        return true;
    }

    function mint(address to,uint256 amount) public  returns(bool){
        require(hasRole(DEFAULT_ADMIN_ROLE,_msgSender()));
        _mint(to,amount);
        return true;
    }
 
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, amount);
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        address spender = _msgSender();
        _spendAllowance(from, spender, amount);
        _transfer(from, to, amount);
        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, allowance(owner, spender) + addedValue);
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        address owner = _msgSender();
        uint256 currentAllowance = allowance(owner, spender);
        require(currentAllowance >= subtractedValue, "ERC20: decreased allowance below zero");
        unchecked {
            _approve(owner, spender, currentAllowance - subtractedValue);
        }
        return true;
    }

    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {
        require(from != address(0), "ERC20: transfer from the zero address");
        require(to != address(0), "ERC20: transfer to the zero address");
        // FINANCIAL
        if(isFinancial(_msgSender())){
            require(
                isFinancial(to)||isEmployee(to),
                "ERC20: allow to transfer from Financial to Financial,Employee"
            );
        }
         // CASHIER_ROLE
        else if(isCashier(_msgSender())){
            require(
                isCashier(to)||isFinancial(to),
                "ERC20: allow to transfer from Cashier to Cashier,Financial"
            );
        }
        // EMPLOYEE_ROLE
        else if(isEmployee(_msgSender())){
            require(
                isCashier(to)||isFinancial(to),
                "ERC20: allow to transfer from Employee to Cashier,Financial"
            );
        }
        // DEFAULT_ADMIN_ROLE
        else if(isAdmin(_msgSender())){
            require(
                !isEmployee(to)||isAdmin(to),
                "ERC20: allow to transfer from Admin to Admin,Financial,Cashier"
            );
        }
        // CUSTOMERS
        else {
            require(
                isCashier(to)||isAdmin(to),
                "ERC20: allow to transfer from Customer to Admin,Cashier"
            );
        }
        _beforeTokenTransfer(from, to, amount);

        uint256 fromBalance = _balances[from];
        require(fromBalance >= amount, "ERC20: transfer amount exceeds balance");
        unchecked {
            _balances[from] = fromBalance - amount;
        }
        _balances[to] += amount;

        emit Transfer(from, to, amount);

        _afterTokenTransfer(from, to, amount);
    }

    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: mint to the zero address");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);

        _afterTokenTransfer(address(0), account, amount);
    }

    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: burn from the zero address");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount, "ERC20: burn amount exceeds balance");
        unchecked {
            _balances[account] = accountBalance - amount;
        }
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);

        _afterTokenTransfer(account, address(0), amount);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _spendAllowance(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        uint256 currentAllowance = allowance(owner, spender);
        if (currentAllowance != type(uint256).max) {
            require(currentAllowance >= amount, "ERC20: insufficient allowance");
            unchecked {
                _approve(owner, spender, currentAllowance - amount);
            }
        }
    }
    // function _grantRole(bytes32 role, address account) internal override virtual {
    //     require(!hasRole(EMPLOYEE_ROLE,account),"remove role employee");
    //     require(!hasRole(FINANCIAL_ROLE,account),"remove role employee");
    //     require(!hasRole(CASHIER_ROLE,account),"remove role employee");
    //     require(!hasRole(DEFAULT_ADMIN_ROLE,account),"remove role employee");
    // }
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    uint256[45] private __gap;
}
