// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

contract Sigs {

    using ECDSA for bytes32;

    mapping(address => mapping(uint256 => bool)) Nonces;
    mapping(address => uint256) MostForNonces;
    address owner;
    
    constructor(address owner_){
        owner = owner_;
    }

    function transfer(uint256 amount,uint256 nonce,bytes memory sig) public  returns(bytes32,address){
        require(!Nonces[msg.sender][nonce]);
        bytes32  message = keccak256(abi.encodePacked(msg.sender, amount, nonce, this)).toEthSignedMessageHash();
        Nonces[msg.sender][nonce] = true;
        MostForNonces[msg.sender]++ ;
        require( message.recover(sig) == owner,"not sig");
        return ( message , message.recover(sig));
    }

    function nonceOf(address  address_) public view returns(uint256){
        return  MostForNonces[address_];
    }
    
    
}
