// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
contract BRS is ERC20Upgradeable,PausableUpgradeable  {

    function initialize(uint256 mint_) public initializer {
         __ERC20_init("Siam Brasseries", "BRS Token",mint_);
         __AccessControl_init();
         __Pausable_init();
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }
}