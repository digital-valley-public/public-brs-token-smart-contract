import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import "hardhat-laika"; 
import "@ericxstone/hardhat-blockscout-verify";
import {SOLIDITY_VERSION, EVM_VERSION} from "@ericxstone/hardhat-blockscout-verify";
dotenv.config();

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();
  for (const account of accounts) {
    console.log(account.address);
  }
});


const config: HardhatUserConfig = {
  defaultNetwork: "gbex",
  networks: {
    kovan: {
      url: "https://kovan.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
      accounts:
        process.env.PRIVATE_KEY !== undefined
          ? [
              "0x53d8adc23948dcbed1ff2bcafb458f158db4e58b2612f1887cf3d5a576801d7e",
            ]
          : [],
      // address 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
    },
    localhost: {
      url: "http://127.0.0.1:8545",
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
      },
    },
    hardhat: {
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
          accountsBalance:"100000000000000000000000000"
      },
      forking: {
        enabled: true,
        url: `https://testnet.gbex.network/`,
      },
    },
    gbex: {
      url: "https://testnet.gbex.network/",
      chainId:11,
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
      }
    }
  },

  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: "JTJXCNN4EIV4MA3ADNSDYRADFWY6GR2C1G",
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts",
  },
  mocha: {
    timeout: 40000,
  },
  solidity: {
   
    compilers: [  { version: "0.4.18" },{ version: "0.8.2" }, { version: "0.8.0" }, { version: "0.8.1" }, ] ,
    settings: {
      optimizer: {
        enabled: true,
        runs: 800,
      },
      metadata: {
        bytecodeHash: "none",
      },
    },
  },
  blockscoutVerify: {
    blockscoutURL: "https://testnet.gbexscan.com/api",
    contracts:{
    }
  },
};

export default config;
