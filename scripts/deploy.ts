import hre, { artifacts } from 'hardhat';
import { Wallet } from 'ethers';
import { ethers } from "hardhat";

async function main() {

  const  SWGBEX = await ethers.getContractFactory("WGBEX");
  const  ERC20 = await ethers.getContractFactory("ERC20");
  let  TVG = await ERC20.deploy("Travizgo Token","TVG",ethers.utils.parseUnits('3000000000', 18));
  let  KAS = await ERC20.deploy("Kas Token","KAS",ethers.utils.parseUnits('3000000000', 18));
  let  GUSD = await ERC20.deploy("GBEX USD","GUSD",ethers.utils.parseUnits('3000000000', 18));
  let  BUSD = await ERC20.deploy("Binance USD","BUSD",ethers.utils.parseUnits('3000000000', 18));
  let  DAI = await ERC20.deploy("Dai Stablecoin","DAI",ethers.utils.parseUnits('3000000000', 18));
  let  ETH = await ERC20.deploy("Ethereum","ETH",ethers.utils.parseUnits('3000000000', 18));
  let  BNB = await ERC20.deploy("Binance","BNB",ethers.utils.parseUnits('3000000000', 18));
  let  WGBEX = await SWGBEX.deploy();
  await WGBEX.deployed()
  await TVG.deployed()
  await KAS.deployed()
  await GUSD.deployed()
  await BUSD.deployed()
  await DAI.deployed()
  await ETH.deployed()
  await BNB.deployed()
  WGBEX.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  TVG.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  KAS.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  GUSD.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  BUSD.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  DAI.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  ETH.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  BNB.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('1000000000', 18))
  console.log(`
    wgbex :   ${WGBEX.address}
    TVG   :   ${TVG.address}
    KAS   :   ${KAS.address}
    GUSD  :   ${GUSD.address}
    BUSD  :   ${BUSD.address}
    DAI   :   ${DAI.address}
    ETH   :   ${ETH.address}
    BNB   :   ${BNB.address}
  `)

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
});


