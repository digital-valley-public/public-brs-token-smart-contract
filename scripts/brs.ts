import hre from 'hardhat';
import { Wallet } from 'ethers';
import { ethers } from "hardhat";

let adminProxsy: Wallet, 
admin1: Wallet,
admin2: Wallet,
employee1: Wallet,
employee2: Wallet,
cashier1: Wallet,
cashier2: Wallet,
adminupgradeabilityProxy:any,
owner:Wallet,
brs:any,
CASHIER_ROLE:string = "0xff3317e1e0e8c784290b957ee9b65ea8fe96420c4819db463283b95ecbe4a3fe",
CUSTOMERS_ROLE:string = "0xaf6786efc154b345802554f1dea27e60ea4c3393b6b38eb8438e22c20e088bd2",
EMPLOYEE_ROLE:string = "0x5a3425ea67e73c95853ecfc0c443062de292a303c186bc2aac1178ae1b86c62a",
DEFAULT_ADMIN_ROLE:string = "0x0000000000000000000000000000000000000000000000000000000000000000",
FINANCIAL_ROLE:string = "0x5245999133c6d8d4571d82773bdec82e2a254d49eae8d487e9867b56643d7333";

async function main() {

    ;[  
        admin1,
        admin2,
        employee1,
        employee2,
        cashier1,
        cashier2,
        adminProxsy,
        owner
      ] = await (ethers as any).getSigners()

  const  AdminUpgradeabilityProxy = await ethers.getContractFactory("AdminUpgradeabilityProxy");
  const  BRS = await ethers.getContractFactory("BRS");
  brs = await  BRS.deploy()
  await brs.deployed();
  
  adminupgradeabilityProxy = await AdminUpgradeabilityProxy.deploy(brs.address,adminProxsy.address,"0x")

  await adminupgradeabilityProxy.deployed();
  
  const brs_token = await ethers.getContractAt("BRS",adminupgradeabilityProxy.address,owner);
  await( await brs_token.initialize(ethers.utils.parseUnits('3000000000', 18))).wait()
  await (await brs_token.transfer("0xfbF2DD4642fc2C50aeDEf7baD7dA73d20c09E961",ethers.utils.parseUnits('100000000', 18))).wait()
  
  console.log(`
    BRS   :   ${brs_token.address}
`)
  await hre.run("laika-sync", {
    contract: "BRS",
    address: adminupgradeabilityProxy.address,
  })
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
});
export default main;

async function checkrole(brs_token:any,ac:string) {
  const t1 =  await brs_token.getRoleMemberCount(CASHIER_ROLE)
  const t2 =  await brs_token.getRoleMemberCount(CUSTOMERS_ROLE)
  const t3 =  await brs_token.getRoleMemberCount(EMPLOYEE_ROLE)
  const t4 =  await brs_token.getRoleMemberCount(FINANCIAL_ROLE)
  const t5 =  await brs_token.getRoleMemberCount(DEFAULT_ADMIN_ROLE)
  console.log('\x1b[33m%s\x1b[0m',`
    ${ac}
      CASHIER_ROLE  ${t1}
      CUSTOMERS_ROLE ${t2}
      EMPLOYEE_ROLE ${t3}
      FINANCIAL_ROLE ${t4}
      DEFAULT_ADMIN_ROLE ${t5}
  `)
}
